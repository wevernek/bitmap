<!DOCTYPE html>
<html lang="pt_BR">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Bitmap - Dashboard</title>
  <link rel="stylesheet" href="../node_modules/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="../node_modules/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="../css/style.css">
</head>

<body>
<?php session_start(); ?>
  <div class="container-scroller">
    <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
        <a class="navbar-brand brand-logo" href="index.php"><img src="../images/logo.png" alt="logo"/></a>
        <a class="navbar-brand brand-logo-mini" href="index.php"><img src="../images/logo-mini.png" alt="logo"/></a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-stretch">
        <ul class="navbar-nav navbar-nav-right">
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle nav-profile" id="profileDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
              <?php
                if (isset($_SESSION['urlFoto_session'])) {
                  $foto = $_SESSION['urlFoto_session'];
                  echo "<img src=". $foto .">";
                }
              ?>
              <span class="d-none d-lg-inline">
                <?php 
                  if (isset($_SESSION['nomeExibicao_session'])) {
                    echo $_SESSION['nomeExibicao_session'];
                  }
                ?>
              </span>
            </a>
            <div class="dropdown-menu navbar-dropdown w-100" aria-labelledby="profileDropdown">
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="logout.method.php">
                <i class="mdi mdi-logout mr-2 text-primary"></i>
                Sair
              </a>
            </div>
          </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
        <span class="mdi mdi-menu"></span>
      </button>
      </div>
    </nav>
    <div class="container-fluid page-body-wrapper">
      <div class="row row-offcanvas row-offcanvas-right">
        <?php include '../components/menu-dashboard.php'; ?>
        <div class="content-wrapper">
          <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <?php
                    include "../config.php";
                    $idGame = $_GET['idGame'];
                    $sql = sprintf("select * from cadastro_jogo where idGame = %s", $idGame);
                    $result = mysqli_query($connect, $sql);
                    $row = mysqli_fetch_array($result);
                  ?>
                  <h4 class="card-title">Edição de jogo</h4>
                  <p class="card-description">
                    Edite um jogo em nosso dashboard
                  </p>
                  <form class="forms-sample" method="POST" action="edit-game.method.php">
                    <div class="form-group">
                      <label for="idGame">ID</label>
                      <input type="text" class="form-control" id="idGame" name="idGame" placeholder="ID do jogo" value="<?php echo $idGame?>">
                    </div>
                    <div class="form-group">
                      <label for="Title">Título</label>
                      <input type="text" class="form-control" id="title" name="title" placeholder="Título do jogo" value="<?php echo $row['title'];?>">
                    </div>
                    <div class="form-group">
                      <label for="ano">Ano</label>
                      <input type="text" class="form-control" id="ano" name="ano" placeholder="Ano" value="<?php echo $row['ano'];?>">
                    </div>
                    <div class="form-group">
                      <label for="producer">Produtora</label>
                      <input type="text" class="form-control" id="producer" name="producer" placeholder="Produtora" value="<?php echo $row['producer'];?>">
                    </div>
                    <div class="form-group">
                      <label for="imagem">URL foto da capa</label>
                      <input type="text" class="form-control" id="imagem" name="imagem" placeholder="URL foto da capa" value="<?php echo $row['imagem'];?>">
                    </div>
                    <div class="form-group">
                      <label for="price">Preço</label>
                      <input type="text" class="form-control" id="price" name="price" placeholder="Preço" value="<?php echo $row['price'];?>">
                    </div>
                    <div class="form-group">
                      <label for="descricao">Descrição</label>
                      <textarea name="descricao" id="descricao" rows="3"><?php echo $row['descricao'];?></textarea>
                    </div>
                    <button type="submit" class="btn btn-success mr-2">Editar jogo</button>
                    <button type="button" class="btn btn-light" onclick="cancel()">Cancelar</button>
                    <!-- <button type="submit" class="btn btn-danger mr-2" style="float: right;">Excluir jogo</button> -->
                  </form>
                  <?php
                    mysqli_free_result($result);
                    mysqli_close($connect);
                  ?>
                </div>
              </div>
            </div>
          </div>
        </div>
        <?php include '../components/footer-dashboard.php'; ?>
      </div>
    </div>
  </div>
  <?php require '../requires/scripts.php'; ?>
</body>

</html>