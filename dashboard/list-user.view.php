<!DOCTYPE html>
<html lang="pt_BR">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Bitmap - Dashboard</title>
  <link rel="stylesheet" href="../node_modules/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="../node_modules/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="../css/style.css">
</head>

<body>
  <?php session_start(); ?>
  <div class="container-scroller">
    <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
        <a class="navbar-brand brand-logo" href="index.php"><img src="../images/logo.png" alt="logo"/></a>
        <a class="navbar-brand brand-logo-mini" href="index.php"><img src="../images/logo-mini.png" alt="logo"/></a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-stretch">
        <ul class="navbar-nav navbar-nav-right">
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle nav-profile" id="profileDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
              <?php
                if (isset($_SESSION['urlFoto_session'])) {
                  $foto = $_SESSION['urlFoto_session'];
                  echo "<img src=". $foto .">";
                }
              ?>
              <span class="d-none d-lg-inline">
                <?php 
                  if (isset($_SESSION['nomeExibicao_session'])) {
                    echo $_SESSION['nomeExibicao_session'];
                  }
                ?>
              </span>
            </a>
            <div class="dropdown-menu navbar-dropdown w-100" aria-labelledby="profileDropdown">
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="logout.method.php">
                <i class="mdi mdi-logout mr-2 text-primary"></i>
                Sair
              </a>
            </div>
          </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
        <span class="mdi mdi-menu"></span>
      </button>
      </div>
    </nav>
    <div class="container-fluid page-body-wrapper">
      <div class="row row-offcanvas row-offcanvas-right">
        <?php include '../components/menu-dashboard.php'; ?>
        <div class="content-wrapper">
          <div class="row">
            <div class="col-12 grid-margin">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Usuários cadastrados</h4>
                  <div class="table-responsive">
                    <table class="table">
                      <thead>
                        <tr>
                          <th>
                            ID
                          </th>
                          <th>
                            Nome do usuário
                          </th>
                          <th>
                            Foto
                          </th>
                          <th>
                            Email
                          </th>
                          <th>
                            
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                          include "../config.php";
                          mysqli_select_db($connect, $db) or print(mysqli_error());
                          $sql = "select * from cadastro_usuario_dashboard";
                          $result = mysqli_query($connect, $sql);
                          
                          while ($row = mysqli_fetch_array($result)) {
                            echo "<tr>";
                            echo "<td>" . $row['idUsuario'] . "</td>";
                            echo "<td>" . $row['nomeExibicao'] . "</td>";
                            echo "<td>";
                              echo "<img src=".  $row['urlFoto'] .">";
                            echo "</td>";
                            echo "<td>" . $row['email'] . "</td>";
                            echo "<td>";
                              echo "<a href='edit-user.view.php?idUsuario=".$row['idUsuario']."'><span class='fa fa-pencil
                              '></span></a>";
                            echo "</td>";
                            echo "<td>";
                              echo "<a href='delete-user.method.php?idUsuario=".$row['idUsuario']."'><span class='fa fa-trash-o
                              '></span></a>";
                            echo "</td>";
                            echo "</tr>";
                          }

                          mysqli_free_result($result);
                          mysqli_close($connect);
                        ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <?php include '../components/footer-dashboard.php'; ?>
      </div>
    </div>
  </div>
  <?php require '../requires/scripts.php'; ?>
</body>

</html>