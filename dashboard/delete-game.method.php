<?php
  include "../config.php";
  $idGame = $_GET["idGame"];

  $sql = sprintf("delete from cadastro_jogo where idGame = %s", $idGame);

  if (mysqli_query($connect, $sql)) {
    header("Location: list-game.view.php");
  } else {
    echo "Erro ao excluir jogo".mysqli_error($connect);
  }
?>