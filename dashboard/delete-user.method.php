<?php
  include "../config.php";
  $idUsuario = $_GET["idUsuario"];

  $sql = sprintf("delete from cadastro_usuario_dashboard where idUsuario = %s", $idUsuario);

  if (mysqli_query($connect, $sql)) {
    header("Location: list-user.view.php");
  } else {
    echo "Erro ao excluir usuário".mysqli_error($connect);
  }
?>