<!DOCTYPE html>
<html lang="pt_BR">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Bitmap - Dashboard</title>
  <link rel="stylesheet" href="../node_modules/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="../node_modules/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="../css/style.css">
</head>

<body>
  <?php session_start(); ?>
  <div class="container-scroller">
    <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
        <a class="navbar-brand brand-logo" href="index.php"><img src="../images/logo.png" alt="logo"/></a>
        <a class="navbar-brand brand-logo-mini" href="index.php"><img src="../images/logo-mini.png" alt="logo"/></a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-stretch">
        <ul class="navbar-nav navbar-nav-right">
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle nav-profile" id="profileDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
              <?php
                if (isset($_SESSION['urlFoto_session'])) {
                  $foto = $_SESSION['urlFoto_session'];
                  echo "<img src=". $foto .">";
                }
              ?>
              <!-- <img src="../images/faces/pedro.jpg" alt="image"> -->
              <span class="d-none d-lg-inline">
                <?php 
                  if (isset($_SESSION['nomeExibicao_session'])) {
                    echo $_SESSION['nomeExibicao_session'];
                  }
                ?>
              </span>
            </a>
            <div class="dropdown-menu navbar-dropdown w-100" aria-labelledby="profileDropdown">
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="logout.method.php">
                <i class="mdi mdi-logout mr-2 text-primary"></i>
                Sair
              </a>
            </div>
          </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
        <span class="mdi mdi-menu"></span>
      </button>
      </div>
    </nav>
    <div class="container-fluid page-body-wrapper">
      <div class="row row-offcanvas row-offcanvas-right">
        <?php include '../components/menu-dashboard.php'; ?>
        <div class="content-wrapper">
          <div class="row">
            <div class="col-md-4 stretch-card grid-margin">
              <div class="card bg-gradient-warning text-white">
                <div class="card-body">
                  <h4 class="font-weight-normal mb-3">Número de vendas</h4>
                  <?php
                    include "../config.php";
                    mysqli_select_db($connect, $db) or print(mysqli_error());
                    $sql = "select * from compra_jogo";
                    $result = mysqli_query($connect, $sql);
                    $rows = mysqli_num_rows($result);
                    echo "<h2 class='font-weight-normal mb-5'>". $rows ."</h2>";
                  ?>
                  <!-- <h2 class="font-weight-normal mb-5">3</h2> -->
                </div>
              </div>
            </div>
            <div class="col-md-4 stretch-card grid-margin">
              <div class="card bg-gradient-info text-white">
                <div class="card-body">
                  <h4 class="font-weight-normal mb-3">Número de jogos</h4>
                  <?php
                    include "../config.php";
                    mysqli_select_db($connect, $db) or print(mysqli_error());
                    $sql = "select * from cadastro_jogo";
                    $result = mysqli_query($connect, $sql);
                    $rows = mysqli_num_rows($result);
                    echo "<h2 class='font-weight-normal mb-5'>". $rows ."</h2>";
                  ?>
                  <!-- <h2 class="font-weight-normal mb-5">4010</h2> -->
                </div>
              </div>
            </div>
            <div class="col-md-4 stretch-card grid-margin">
              <div class="card bg-gradient-success text-white">
                <div class="card-body">
                  <h4 class="font-weight-normal mb-3">Número de usuários</h4>
                  <?php
                    include "../config.php";
                    mysqli_select_db($connect, $db) or print(mysqli_error());
                    $sql = "select * from cadastro_usuario_ecommerce";
                    $result = mysqli_query($connect, $sql);
                    $rows = mysqli_num_rows($result);
                    echo "<h2 class='font-weight-normal mb-5'>". $rows ."</h2>";
                  ?>
                  <!-- <h2 class="font-weight-normal mb-5">3</h2> -->
                </div>
              </div>
            </div>
          </div>
        </div>
        <?php include '../components/footer-dashboard.php'; ?>
      </div>
    </div>
  </div>
  <?php require '../requires/scripts.php'; ?>
</body>

</html>