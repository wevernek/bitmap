<!DOCTYPE html>
<html lang="pt_BR">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Bitmap - Dashboard</title>
  <link rel="stylesheet" href="../node_modules/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="../node_modules/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="../css/style.css">
</head>

<body>
  <?php session_start(); ?>
  <div class="container-scroller">
    <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
        <a class="navbar-brand brand-logo" href="index.php"><img src="../images/logo.png" alt="logo"/></a>
        <a class="navbar-brand brand-logo-mini" href="index.php"><img src="../images/logo-mini.png" alt="logo"/></a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-stretch">
        <ul class="navbar-nav navbar-nav-right">
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle nav-profile" id="profileDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
              <?php
                if (isset($_SESSION['urlFoto_session'])) {
                  $foto = $_SESSION['urlFoto_session'];
                  echo "<img src=". $foto .">";
                }
              ?>
              <span class="d-none d-lg-inline">
                <?php 
                  if (isset($_SESSION['nomeExibicao_session'])) {
                    echo $_SESSION['nomeExibicao_session'];
                  }
                ?>
              </span>
            </a>
            <div class="dropdown-menu navbar-dropdown w-100" aria-labelledby="profileDropdown">
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="logout.method.php">
                <i class="mdi mdi-logout mr-2 text-primary"></i>
                Sair
              </a>
            </div>
          </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
        <span class="mdi mdi-menu"></span>
      </button>
      </div>
    </nav>
    <div class="container-fluid page-body-wrapper">
      <div class="row row-offcanvas row-offcanvas-right">
        <?php include '../components/menu-dashboard.php'; ?>
        <div class="content-wrapper">
          <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <?php
                    include "../config.php";
                    $idUsuario = $_GET['idUsuario'];
                    $sql = sprintf("select * from cadastro_usuario_dashboard where idUsuario = %s", $idUsuario);
                    $result = mysqli_query($connect, $sql);
                    $row = mysqli_fetch_array($result);
                  ?>
                  <h4 class="card-title">Edição de usuário</h4>
                  <p class="card-description">
                    Edite um usuário em nosso dashboard
                  </p>
                  <form class="forms-sample" method="POST" action="edit-user.method.php">
                    <div class="form-group">
                      <label for="title">ID</label>
                      <input type="text" class="form-control" id="idUsuario" name="idUsuario" placeholder="Nome" value="<?php echo $idUsuario?>">
                    </div>
                    <div class="form-group">
                      <label for="title">Nome</label>
                      <input type="text" class="form-control" id="nome" name="nome" placeholder="Nome" value="<?php echo $row['nome'];?>">
                    </div>
                    <div class="form-group">
                      <label for="year">Sobrenome</label>
                      <input type="text" class="form-control" id="sobrenome" name="sobrenome" placeholder="Sobrenome" value="<?php echo $row['sobrenome'];?>">
                    </div>
                    <div class="form-group">
                      <label for="producer">Nome para exibição</label>
                      <input type="text" class="form-control" id="nomeExibicao" name="nomeExibicao" placeholder="Nome para exibição" value="<?php echo $row['nomeExibicao'];?>">
                    </div>
                    <div class="form-group">
                      <label for="image">URL foto de perfil</label>
                      <input type="text" class="form-control" id="urlFoto" name="urlFoto" placeholder="Foto de perfil" value="<?php echo $row['urlFoto'];?>">
                    </div>
                    <div class="form-group">
                      <label for="price">Email</label>
                      <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="<?php echo $row['email'];?>">
                    </div>
                    <div class="form-group">
                      <label for="price">Senha</label>
                      <input type="password" class="form-control" id="senha" name="senha" placeholder="Senha" value="<?php echo $row['senha'];?>">
                    </div>
                    <div class="form-group">
                      <label for="price">Repita a senha</label>
                      <input type="password" class="form-control" id="repitaSenha" name="repitaSenha" placeholder="Repita a senha" value="<?php echo $row['repitaSenha'];?>">
                    </div>
                    <button type="submit" class="btn btn-success mr-2">Editar usuário</button>
                    <button type="button" class="btn btn-light" onclick="cancel()">Cancelar</button>
                    <!-- <button type="submit" class="btn btn-danger mr-2" style="float: right;">Excluir usuário</button> -->
                  </form>
                  <?php
                    mysqli_free_result($result);
                    mysqli_close($connect);
                  ?>
                </div>
              </div>
            </div>
          </div>
        </div>
        <?php include '../components/footer-dashboard.php'; ?>
      </div>
    </div>
  </div>
  <?php require '../requires/scripts.php'; ?>
</body>

</html>