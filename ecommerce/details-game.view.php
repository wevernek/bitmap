<!DOCTYPE html>
<html>
<head>
	<title>Bitmap</title>
	<?php include '../components/head.php'; ?>
</head>
<body>
	<?php session_start(); ?>
	<nav class="navbar navbar-expand-lg navbar-light static-top sb-navbar">
		<div class="container">
			<a class="navbar-brand" href="index.php">Bitmap</a>
			<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
			Menu
			<i class="fa fa-bars"></i>
			</button>
			<div class="collapse navbar-collapse" id="navbarResponsive">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item">
						<a class="nav-link" href="index.php" title="Home">Home</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="contact.view.php" title="Contato">Contato</a>
					</li>
				</ul>
				<ul class="navbar-nav">
					<?php
						include "../config.php";
						mysqli_select_db($connect, $db) or print(mysqli_error());
						$sql = "select * from cadastro_usuario_ecommerce";
						$result = mysqli_query($connect, $sql);
						$row = mysqli_fetch_array($result);

						echo "<div class='login-infos'>";
						echo "<a href='details-profile.view.php?idUsuario=".$row['idUsuario']."'>";
						echo "<div class='photo-container'>";
						if (isset($_SESSION['fotoUrl_session'])) {
							$foto = $_SESSION['fotoUrl_session'];
							echo "<img src=". $foto .">";
						}
						echo "</div>";
						if (isset($_SESSION['nomeCompleto_session'])) {
							echo $_SESSION['nomeCompleto_session'];
							echo "<a href='logout.method.php'><span class='fa fa-sign-out' style='margin-left: 10px;'></span></a>";
						} else {
							echo "<li class='nav-item login'>";
								echo "<a class='nav-link' href='login.view.php'>Login</a>";
							echo "</li>";
						}
						echo "</a>";
						echo "</div>";
					?>
				</ul>
			</div>
		</div>
	</nav>
	<div class="container">
    <div class="card-detail">
      <?php
        include "../config.php";
        $idGame = $_GET['idGame'];
        $sql = sprintf("select * from cadastro_jogo where idGame = %s", $idGame);
        $result = mysqli_query($connect, $sql);
        
        function asReal($value) {
          return 'R$' . number_format($value, 2, ',', '.');
        }
        
        while ($row = mysqli_fetch_array($result)) {
          $priceNoFormated = $row['price'];
          $priceFormated = asReal($priceNoFormated);
          echo "<div class='row'>";
            echo "<div class='col col-md-6'>";
              echo "<div class='game-image'>";
                echo "<img src=". $row['imagem'] .">";
              echo "</div>";
            echo "</div>";
            echo "<div class='col col-md-6'>";
              echo "<div class='game-infos'>";
                echo "<h3>" . $row['title'] . "</h3>";
                echo "<h6>" . $row['producer'] . "</h6>";
                echo "<p class='year'>" . $row['ano'] . "</p>";
                echo "<p class='description'>" . $row['descricao'] . "</p>";
								echo "<p class='price'>" . $priceFormated . "</p>";
								if (isset($_SESSION['nomeCompleto_session'])) {
									echo "<a href='purchase-game.view.php?idGame=".$row['idGame']."' class='button-purchase'>Comprar jogo</a>";
								} else {
									echo "<a href='login.view.php'>Faça login para comprar</a>";
								}
                
              echo "</div>";
            echo "</div>";
          echo "</div>";
        }

        mysqli_free_result($result);
        mysqli_close($connect);
      ?>
    </div>
	</div>
</body>
</html>