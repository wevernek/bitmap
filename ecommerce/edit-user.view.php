<!DOCTYPE html>
<html>
<head>
	<title>Bitmap</title>
	<?php include '../components/head.php'; ?>
</head>
<body>
  <?php
    include "../config.php";
    $idUsuario = $_GET['idUsuario'];
    $sql = sprintf("select * from cadastro_usuario_ecommerce where idUsuario = %s", $idUsuario);
    $result = mysqli_query($connect, $sql);
    $row = mysqli_fetch_array($result);
  ?>
	<div class="container">
    <form method="POST" action="edit-user.method.php">
      <div class="row">
      <div class="col col-md-12">
        <div class="row">
          <div class="col col-md-6">
            <div class="form-group">
              <label for="nomeCompleto">ID</label>
              <input type="number" class="form-control" id="idUsuario" name="idUsuario" value="<?php echo $row['idUsuario'] ?>" readonly>
            </div>
          </div>
          <div class="col col-md-6">
            <div class="form-group">
              <label for="nomeCompleto">Nome completo</label>
              <input type="text" class="form-control" id="nomeCompleto" name="nomeCompleto" value="<?php echo $row['nomeCompleto'] ?>" required>
            </div>
          </div>
          <div class="col col-md-6">
            <div class="form-group">
              <label for="fotoUrl">URL foto de perfil</label>
              <input type="text" class="form-control" id="fotoUrl" name="fotoUrl" value="<?php echo $row['fotoUrl'] ?>" required>
            </div>
          </div>
          <div class="col col-md-6">
            <div class="form-group">
              <label for="email">Email</label>
              <input type="email" class="form-control" id="email" name="email" value="<?php echo $row['email'] ?>" required>
            </div>
          </div>
          <div class="col col-md-6">
            <div class="form-group">
              <label for="senha">Senha</label>
              <input type="password" class="form-control" id="senha" name="senha" value="<?php echo $row['senha'] ?>" required>
            </div>
          </div>
          <div class="col col-md-6">
            <div class="form-group">
              <label for="endereco">Endereço</label>
              <input type="text" class="form-control" id="endereco" name="endereco" value="<?php echo $row['endereco'] ?>" required>
            </div>
          </div>
          <div class="col col-md-2">
            <div class="form-group">
              <label for="numero">Número</label>
              <input type="number" class="form-control" id="numero" name="numero" value="<?php echo $row['numero'] ?>" required>
            </div>
          </div>
          <div class="col col-md-4">
            <div class="form-group">
              <label for="bairro">Bairro</label>
              <input type="text" class="form-control" id="bairro" name="bairro" value="<?php echo $row['bairro'] ?>" required>
            </div>
          </div>
          <div class="col col-md-6">
            <div class="form-group">
              <label for="estado">Estado</label>
              <input type="text" class="form-control" id="estado" name="estado" value="<?php echo $row['estado'] ?>" required>
            </div>
          </div>
          <div class="col col-md-6">
            <div class="form-group">
              <label for="cidade">Cidade</label>
              <input type="text" class="form-control" id="cidade" name="cidade" value="<?php echo $row['cidade'] ?>" required>
            </div>
          </div>
          <div class="col col-md-6">
            <div class="form-group">
              <label for="numCartao">Número do cartão</label>
              <input type="number" class="form-control" id="numCartao" name="numCartao" value="<?php echo $row['numCartao'] ?>" required>
            </div>
          </div>
          <div class="col col-md-6">
            <div class="form-group">
              <label for="nomeCartao">Nome no cartão</label>
              <input type="text" class="form-control" id="nomeCartao" name="nomeCartao" value="<?php echo $row['nomeCartao'] ?>" required>
            </div>
          </div>
          <div class="col col-md-6">
            <div class="form-group">
              <label for="dataExp">Data de expiração</label>
              <input type="text" class="form-control" id="dataExp" name="dataExp" value="<?php echo $row['dataExp'] ?>" required>
            </div>
          </div>
          <div class="col col-md-6">
            <div class="form-group">
              <label for="codSeg">Código de segurança</label>
              <input type="number" class="form-control" id="codSeg" name="codSeg" value="<?php echo $row['codSeg'] ?>" required>
            </div>
          </div>
        </div>
      </div>
      </div>
      <button type="button" class="btn btn-default" onclick="backToDetailsProfile()">Cancelar</button>
      <button type="submit" class="btn btn-primary">Salvar</button>
    </form>
	</div>
</body>
</html>