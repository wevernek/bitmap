<!DOCTYPE html>
<html>
<head>
	<title>Bitmap - Login</title>
	<?php include '../components/head.php'; ?>
</head>
<body>
  <script>
    function redirectPage() {
      window.location.href = 'index.php';
    }
  </script>
	<div class="container home" style="height: 100vh; display: flex; align-items: center; justify-content: center; flex-direction: column;">
    <form method="POST" action="login.method.php">
      <img src="../images/logo.png" width="200px" alt="">
      <div class="card list-games">
        <div class="card-body">
          <div class="form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control" id="email" name="email" placeholder="Email">
          </div>
          <div class="form-group">
            <label for="senha">Senha</label>
            <input type="password" class="form-control" id="senha" name="senha" placeholder="Senha">
          </div>
          <a href="register-user.view.php" class="d-block text-center">Registrar-se</a>
        </div>
      </div>
      <button type="button" class="btn btn-default mt-3 float-left" onclick="redirectPage()">Voltar</button>
      <button type="submit" class="btn btn-primary mt-3 float-right">Entrar</button>
    </form>
	</div>
</body>
</html>