<!DOCTYPE html>
<html>
<head>
	<title>Bitmap</title>
	<?php include '../components/head.php'; ?>
</head>
<body>
	<div class="container">
    <form method="POST" action="register-user.method.php">
      <div class="row">
        <div class="col col-md-6">
          <div class="form-group">
            <label for="nomeCompleto">Nome completo</label>
            <input type="text" class="form-control" id="nomeCompleto" name="nomeCompleto" required>
          </div>
        </div>
        <div class="col col-md-6">
          <div class="form-group">
            <label for="fotoUrl">URL foto do perfil</label>
            <input type="text" class="form-control" id="fotoUrl" name="fotoUrl" required>
          </div>
        </div>
        <div class="col col-md-6">
          <div class="form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control" id="email" name="email" required>
          </div>
        </div>
        <div class="col col-md-6">
          <div class="form-group">
            <label for="senha">Senha</label>
            <input type="password" class="form-control" id="senha" name="senha" required>
          </div>
        </div>
        <div class="col col-md-6">
          <div class="form-group">
            <label for="endereco">Endereço</label>
            <input type="text" class="form-control" id="endereco" name="endereco" required>
          </div>
        </div>
        <div class="col col-md-2">
          <div class="form-group">
            <label for="numero">Número</label>
            <input type="number" class="form-control" id="numero" name="numero" required>
          </div>
        </div>
        <div class="col col-md-4">
          <div class="form-group">
            <label for="bairro">Bairro</label>
            <input type="text" class="form-control" id="bairro" name="bairro" required>
          </div>
        </div>
        <div class="col col-md-6">
          <div class="form-group">
            <label for="estado">Estado</label>
            <input type="text" class="form-control" id="estado" name="estado" required>
          </div>
        </div>
        <div class="col col-md-6">
          <div class="form-group">
            <label for="cidade">Cidade</label>
            <input type="text" class="form-control" id="cidade" name="cidade" required>
          </div>
        </div>
        <div class="col col-md-6">
          <div class="form-group">
            <label for="numCartao">Número do cartão</label>
            <input type="number" class="form-control" id="numCartao" name="numCartao" required>
          </div>
        </div>
        <div class="col col-md-6">
          <div class="form-group">
            <label for="nomeCartao">Nome no cartão</label>
            <input type="text" class="form-control" id="nomeCartao" name="nomeCartao" required>
          </div>
        </div>
        <div class="col col-md-6">
          <div class="form-group">
            <label for="dataExp">Data de expiração</label>
            <input type="text" class="form-control" id="dataExp" name="dataExp" required>
          </div>
        </div>
        <div class="col col-md-6">
          <div class="form-group">
            <label for="codSeg">Código de segurança</label>
            <input type="number" class="form-control" id="codSeg" name="codSeg" required>
          </div>
        </div>
      </div>
      <button type="submit" class="btn btn-primary">Cadastrar</button>
    </form>
	</div>
</body>
</html>