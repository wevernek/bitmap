<!DOCTYPE html>
<html>
<head>
	<title>Bitmap</title>
	<?php include '../components/head.php'; ?>
</head>
<body>
  <?php session_start(); ?>
	<nav class="navbar navbar-expand-lg navbar-light static-top sb-navbar">
		<div class="container">
			<a class="navbar-brand" href="index.php">Bitmap</a>
			<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
			Menu
			<i class="fa fa-bars"></i>
			</button>
			<div class="collapse navbar-collapse" id="navbarResponsive">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item">
						<a class="nav-link" href="index.php" title="Home">Home</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="contact.view.php" title="Contato">Contato</a>
					</li>
				</ul>
				<ul class="navbar-nav">
					<?php
						include "../config.php";
						mysqli_select_db($connect, $db) or print(mysqli_error());
						$sql = "select * from cadastro_usuario_ecommerce";
						$result = mysqli_query($connect, $sql);
						$row = mysqli_fetch_array($result);

						echo "<div class='login-infos'>";
						echo "<a href='details-profile.view.php?idUsuario=".$row['idUsuario']."'>";
						echo "<div class='photo-container'>";
						if (isset($_SESSION['fotoUrl_session'])) {
							$foto = $_SESSION['fotoUrl_session'];
							echo "<img src=". $foto .">";
						}
						echo "</div>";
						if (isset($_SESSION['nomeCompleto_session'])) {
							echo $_SESSION['nomeCompleto_session'];
							echo "<a href='logout.method.php'><span class='fa fa-sign-out' style='margin-left: 10px;'></span></a>";
						} else {
							echo "<li class='nav-item login'>";
								echo "<a class='nav-link' href='login.view.php'>Login</a>";
							echo "</li>";
						}
						echo "</a>";
						echo "</div>";
					?>
				</ul>
			</div>
		</div>
	</nav>
	<div class="container home">
		<div class="card list-games">
			<div class="card-body">
        <form action="contact.method.php" method="POST">
          <div class="form-group">
            <label for="nome">Nome</label>
            <input type="text" class="form-control" id="nome" name="nome" placeholder="Nome">
          </div>
          <div class="form-group">
            <label for="email">Email</label>
            <input type="text" class="form-control" id="email" name="email" placeholder="Email">
          </div>
          <div class="form-group">
            <label for="mensagem">Mensagem</label>
            <textarea class="form-control" id="mensagem" name="mensagem" rows="3"></textarea>
          </div>
          <button type="reset" class="btn btn-primary">Limpar</button>
          <button type="submit" class="btn btn-primary">Enviar</button>
        </form>
			</div>
		</div>
	</div>
</body>
</html>