<!DOCTYPE html>
<html>
<head>
	<title>Bitmap</title>
	<?php include '../components/head.php'; ?>
</head>
<body>
	<?php session_start(); ?>
	<nav class="navbar navbar-expand-lg navbar-light static-top sb-navbar">
		<div class="container">
			<a class="navbar-brand" href="index.php">Bitmap</a>
			<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
			Menu
			<i class="fa fa-bars"></i>
			</button>
			<div class="collapse navbar-collapse" id="navbarResponsive">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item">
						<a class="nav-link" href="index.php" title="Home">Home</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="contact.view.php" title="Contato">Contato</a>
					</li>
				</ul>
				<ul class="navbar-nav">
					<?php
						include "../config.php";
						mysqli_select_db($connect, $db) or print(mysqli_error());
						$sql = "select * from cadastro_usuario_ecommerce";
						$result = mysqli_query($connect, $sql);
						$row = mysqli_fetch_array($result);

						echo "<div class='login-infos'>";
						echo "<a href='details-profile.view.php?idUsuario=".$row['idUsuario']."'>";
						echo "<div class='photo-container'>";
						if (isset($_SESSION['fotoUrl_session'])) {
							$foto = $_SESSION['fotoUrl_session'];
							echo "<img src=". $foto .">";
						}
						echo "</div>";
						if (isset($_SESSION['nomeCompleto_session'])) {
							echo $_SESSION['nomeCompleto_session'];
							echo "<a href='logout.method.php'><span class='fa fa-sign-out' style='margin-left: 10px;'></span></a>";
						} else {
							echo "<li class='nav-item login'>";
								echo "<a class='nav-link' href='login.view.php'>Login</a>";
							echo "</li>";
						}
						echo "</a>";
						echo "</div>";
					?>
				</ul>
			</div>
		</div>
	</nav>
	<div class="container home">
		<div class="card slide-games">
			<div class="card-body p-0">
				<?php include '../components/slide.component.php'; ?>
			</div>
		</div>
		<div class="card list-games">
			<div class="card-body">
				<div class="row">
					<?php
						include "../config.php";
						mysqli_select_db($connect, $db) or print(mysqli_error());
						$sql = "select * from cadastro_jogo";
						$result = mysqli_query($connect, $sql);

						function asReal($value) {
							return 'R$' . number_format($value, 2, ',', '.');
						}
						
						while ($row = mysqli_fetch_array($result)) {
							$priceNoFormated = $row['price'];
							$priceFormated = asReal($priceNoFormated);
							echo "<div class='item-game col-xs-12 col-sm-6 col-md-4 col-lg-3'>";
							echo "<a href='details-game.view.php?idGame=".$row['idGame']."'>";
							echo "<img src=". $row['imagem'] ." width='200px'>";
							echo "<h3>" . $row['title'] . "</h3>";
							echo "<h6>" . $row['producer'] . "</h6>";
							echo "<p>" . $priceFormated . "</p>";
							echo "</a>";
							echo "</div>";
						}

						mysqli_free_result($result);
						mysqli_close($connect);
					?>
				</div>
			</div>
		</div>
	</div>
</body>
</html>