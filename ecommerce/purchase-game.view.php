<!DOCTYPE html>
<html>
<head>
	<title>Bitmap</title>
	<?php include '../components/head.php'; ?>
</head>
<body>
  <?php session_start(); ?>
  <?php
    include "../config.php";
    $idGame = $_GET['idGame'];
    $sql = sprintf("select * from cadastro_jogo where idGame = %s", $idGame);
    $result = mysqli_query($connect, $sql);
    $row = mysqli_fetch_array($result);
  ?>
	<div class="container">
    <form method="POST" action="purchase-game.method.php">
      <div class="row">
      <div class="col col-md-6" style="background: #EFEFEF; display: flex; align-items: center; justify-content: center; flex-direction: column;">
        <?php
          function asReal($value) {
            return 'R$' . number_format($value, 2, ',', '.');
          }
        
          $priceNoFormated = $row['price'];
          $priceFormated = asReal($priceNoFormated);
          echo "<img src=". $row['imagem'] ." width='200px'>";
          echo "<h3>". $row['title'] ."</h3>";
          echo "<h6>". $row['producer'] ."</h6>";
          echo "<p>". $row['ano'] ."</p>";
          echo "<h1>". $priceFormated."</h1>";
        ?>
      </div>
      <div class="col col-md-6">
        <div class="row">
          <div class="col col-md-6">
            <div class="form-group">
              <label for="endereco">Endereço</label>
              <input type="text" class="form-control" id="endereco" name="endereco" value="<?php if (isset($_SESSION['endereco_session'])) { echo $_SESSION['endereco_session']; } ?>" required>
            </div>
          </div>
          <div class="col col-md-2">
            <div class="form-group">
              <label for="numero">Número</label>
              <input type="number" class="form-control" id="numero" name="numero" value="<?php if (isset($_SESSION['numero_session'])) { echo $_SESSION['numero_session']; } ?>" required>
            </div>
          </div>
          <div class="col col-md-4">
            <div class="form-group">
              <label for="bairro">Bairro</label>
              <input type="text" class="form-control" id="bairro" name="bairro" value="<?php if (isset($_SESSION['bairro_session'])) { echo $_SESSION['bairro_session']; } ?>" required>
            </div>
          </div>
          <div class="col col-md-6">
            <div class="form-group">
              <label for="estado">Estado</label>
              <input type="text" class="form-control" id="estado" name="estado" value="<?php if (isset($_SESSION['estado_session'])) { echo $_SESSION['estado_session']; } ?>" required>
            </div>
          </div>
          <div class="col col-md-6">
            <div class="form-group">
              <label for="cidade">Cidade</label>
              <input type="text" class="form-control" id="cidade" name="cidade" value="<?php if (isset($_SESSION['cidade_session'])) { echo $_SESSION['cidade_session']; } ?>" required>
            </div>
          </div>
          <div class="col col-md-6">
            <div class="form-group">
              <label for="numCartao">Número do cartão</label>
              <input type="number" class="form-control" id="numCartao" name="numCartao" value="<?php if (isset($_SESSION['numCartao_session'])) { echo $_SESSION['numCartao_session']; } ?>" required>
            </div>
          </div>
          <div class="col col-md-6">
            <div class="form-group">
              <label for="nomeCartao">Nome no cartão</label>
              <input type="text" class="form-control" id="nomeCartao" name="nomeCartao" value="<?php if (isset($_SESSION['nomeCartao_session'])) { echo $_SESSION['nomeCartao_session']; } ?>" required>
            </div>
          </div>
          <div class="col col-md-6">
            <div class="form-group">
              <label for="dataExp">Data de expiração</label>
              <input type="text" class="form-control" id="dataExp" name="dataExp" value="<?php if (isset($_SESSION['dataExp_session'])) { echo $_SESSION['dataExp_session']; } ?>" required>
            </div>
          </div>
          <div class="col col-md-6">
            <div class="form-group">
              <label for="codSeg">Código de segurança</label>
              <input type="number" class="form-control" id="codSeg" name="codSeg" value="<?php if (isset($_SESSION['codSeg_session'])) { echo $_SESSION['codSeg_session']; } ?>" required>
            </div>
          </div>
          <div class="col col-md-6">
            <div class="form-group">
              <label for="quantidade">Quantidade de jogos</label>
              <select name="quantidade" id="quantidade">
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="9">9</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
              </select>
            </div>
          </div>
          <div class="col col-md-6">
            <div class="form-group">
              <label for="parcelas">Quantidade de parcelas</label>
              <select name="parcelas" id="parcelas">
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="9">9</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
              </select>
            </div>
          </div>
        </div>
      </div>
      </div>
      <button type="submit" class="btn btn-primary">Comprar</button>
    </form>
	</div>
</body>
</html>