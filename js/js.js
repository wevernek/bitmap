function validForm() {
  if (document.getElementById('senha').value !== document.getElementById('repitaSenha').value) {
    alert ('As senhas não são iguais! Digite novamente!');
    return false;
  }
}

function cancel() {
  window.location.href = 'index.php';
}

function backToDetailsProfile() {
  window.history.back();
}