<?php
  echo '
    <nav class="sidebar sidebar-offcanvas" id="sidebar">
      <ul class="nav">
        <li class="nav-item">
          <a class="nav-link" href="index.php">
            <span class="menu-title">Home</span>
            <i class="mdi mdi-home menu-icon"></i>
          </a>
        </li>
        <li class="nav-item" id="bitmap">
          <a class="nav-link" href="../ecommerce/index.php">
            <span class="menu-title">Bitmap</span>
            <i class=" menu-icon"></i>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="collapse" href="#game" aria-expanded="false" aria-controls="game">
            <span class="menu-title">Jogos</span>
            <i class="menu-arrow"></i>
            <i class="mdi mdi-format-list-bulleted menu-icon"></i>
          </a>
          <div class="collapse" id="game">
            <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link" href="../dashboard/list-game.view.php"> Lista de jogos </a></li>
              <li class="nav-item"> <a class="nav-link" href="../dashboard/register-game.view.php"> Cadastrar jogo </a></li>
            </ul>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="collapse" href="#user" aria-expanded="false" aria-controls="user">
            <span class="menu-title">Usuários</span>
            <i class="menu-arrow"></i>
            <i class="mdi mdi-contacts menu-icon"></i>
          </a>
          <div class="collapse" id="user">
            <ul class="nav flex-column sub-menu">
              <li class="nav-item"> <a class="nav-link" href="../dashboard/list-user.view.php"> Lista de usuários </a></li>
              <li class="nav-item"> <a class="nav-link" href="../dashboard/register-user.view.php"> Cadastrar usuário </a></li>
            </ul>
          </div>
        </li>
      </ul>
    </nav>
  '
?>