<footer class="footer">
  <div class="d-sm-flex justify-content-center justify-content-sm-between">
    <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2018 <a href="#" target="_blank">Bitmap</a>. Todos os direitos reservados.</span>
    <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Desenvolvido com <i class="mdi mdi-heart text-danger"></i> por Pedro e Denilson</span>
  </div>
</footer>