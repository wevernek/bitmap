<?php
	echo '
		<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
			<ol class="carousel-indicators">
				<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
				<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
				<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
			</ol>
			<div class="carousel-inner">
				<div class="carousel-item active">
					<img class="d-block w-100" src="https://www.torcedores.com/content/uploads/2015/02/GTA.jpg" alt="First slide">
					<div class="carousel-caption d-none d-md-block">
						<h5>First slide</h5>
						<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Necessitatibus suscipit voluptas iste voluptatem! Qui, magnam reprehenderit! Eveniet magnam laboriosam aut consectetur, totam, dicta repudiandae odio impedit porro recusandae in est.</p>
					</div>
				</div>
				<div class="carousel-item">
					<img class="d-block w-100" src="https://www.torcedores.com/content/uploads/2015/02/GTA.jpg" alt="Second slide">
					<div class="carousel-caption d-none d-md-block">
						<h5>Secondo slide</h5>
						<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Necessitatibus suscipit voluptas iste voluptatem! Qui, magnam reprehenderit! Eveniet magnam laboriosam aut consectetur, totam, dicta repudiandae odio impedit porro recusandae in est.</p>
					</div>
				</div>
				<div class="carousel-item">
					<img class="d-block w-100" src="https://www.torcedores.com/content/uploads/2015/02/GTA.jpg" alt="Third slide">
					<div class="carousel-caption d-none d-md-block">
						<h5>Third slide</h5>
						<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Necessitatibus suscipit voluptas iste voluptatem! Qui, magnam reprehenderit! Eveniet magnam laboriosam aut consectetur, totam, dicta repudiandae odio impedit porro recusandae in est.</p>
					</div>
				</div>
			</div>
			<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
				<span class="carousel-control-next-icon" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>
	';
?>